# LAB 1 - node project on Heroku with Gitlab-CI

Small lab project for students to test how easy is to deploy and manage applications on PaaS. This will be also usefull to show them the difference between IaaS , PaaS and SaaS.

Before doing the task, try to start the app locally( it will be noted :D ). to do this just do all the steps below, but carreful, nothing is worse than something, so keep in mind doing the lab too.

Good luck! 


 ## Tasks
 Remember, this code must be uploaded to your gitlab account(just follow the steps on your newly created app in gitlab).
 - update the `.gitlab-ci.yml` file and change the `$MY_APP_NAME` by your app name in heroku
 - update the `.gitlab-ci.yml` file and change the `$MY_API_KEY` by your api key in heroku
 - go to Gitlab under ci/cd and if there is something red call me :)
 - if everything is good you should be able to see your app running on Heroku. just click on `open app` in your heroku app
 - **Extra**: update your `index.js` and add something cool ( everything counts, surprise me ;D )

 ## Dependences
- to start, `node`, i advice you to use `nvm` (https://github.com/nvm-sh/nvm) and then `nvm install node --lts`
 
 ## Before start
 `npm install`
 
 ## Run the server
 `npm start`
 